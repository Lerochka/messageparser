#include <QCoreApplication>
#include <QTextCodec>
#include <iostream>
#include <sstream>


using namespace std;

//Структура системы датчиков
struct response
{
    float kHeading = 0;
    float kPitch = 0;
    float kRoll = 0;
    char kHeadingStatus = ' ';
    float kQuaternion[4] = {0,0,0,0}; // 4 числа типа float
    float kTemperature = 0;
    bool kDistortion = false;
    bool kCalStatus = false;
    float kAccelX= 0;
    float kAccelY = 0;
    float kAccelZ = 0;
    float kMagX = 0;
    float kMagY = 0;
    float kMagZ = 0;
    float kGyroX = 0;
    float kGyroY = 0;
    float kGyroZ = 0;
};

// объявление функций
response messageToResponse(string message);
int hexToInt(string hexVal);
void setField(response &resp, int id, string hexCode);
float hexToFloat(string hexVal);
void printResponse(response resp);


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    response resp; // создаем пустую структуру

    //задание строки для дешифровки
    string message = "05430D083B183F91BCD8193EDF4DE94F00080015BCA408C0163BFB4DAB173F80F5081BC16EA4371cc12d31271d42513d4f4a000000004b000000004c00000000";

    //тут получим готовую структуру
    resp = messageToResponse(message);

    //печать нашей структуры
    printResponse(resp);

    return a.exec();
}


response messageToResponse(string message)
{
    int i = 0;
    string hexCode;
    response resp;

    while(i < message.size())
    {
        // берем первые два символа и переводим из 16ричной в десятичную систему
        int id = hexToInt(message.substr(i,2));

        string code;
        if (id == 77) {
            hexCode = message.substr(i + 2, 32);
            i += 34;
        } else if(id == 8 || id == 9 || id == 79) {
            hexCode = message.substr(i + 2, 2);
            i += 4;
        } else {
            hexCode = message.substr(i + 2, 8);
            i += 10;
        }
        //результатом работы верхнего блока будет id в 10той и сообщение в 16ричной

        //метод установки данных в структуру
        setField(resp, id, hexCode);
    }
    return resp;
}

//перевод из 16ричной в 10ную
int hexToInt(string hexVal)
{
    //переводим строку в массив char
    char cstr[hexVal.size() + 1];
    hexVal.copy(cstr, hexVal.size() + 1);
    cstr[hexVal.size()] = '\0';

    //переодим массив чаров в int
    uint32_t num;
    int i;
    sscanf(cstr, "%x", &num);
    i = *((int*)&num);
    return i;
}

//метод установки данных в структуру
void setField(response &resp, int id, string hexCode)
{
    switch (id) {
    case 5:
        resp.kHeading = hexToFloat(hexCode);
        break;
    case 24:
        resp.kPitch = hexToFloat(hexCode);
        break;
    case 25:
        resp.kRoll = hexToFloat(hexCode);
        break;
    case 77:
        resp.kQuaternion[0] = hexToFloat(hexCode.substr(0,8));
        resp.kQuaternion[1] = hexToFloat(hexCode.substr(8,16));
        resp.kQuaternion[2] = hexToFloat(hexCode.substr(16,24));
        resp.kQuaternion[3] = hexToFloat(hexCode.substr(24,32));
        break;
    case 7:
        resp.kTemperature = hexToFloat(hexCode);
        break;
    case 8:
        if(hexCode == "00") {
            resp.kDistortion = false;
        } else {
            resp.kDistortion = true;
        }
        break;
    case 9:
        if(hexCode == "00") {
            resp.kCalStatus = false;
        } else {
            resp.kCalStatus = true;
        }
        break;
    case 21:
        resp.kAccelX = hexToFloat(hexCode);
        break;
    case 22:
        resp.kAccelY = hexToFloat(hexCode);
        break;
    case 23:
        resp.kAccelZ = hexToFloat(hexCode);
        break;
    case 27:
        resp.kMagX = hexToFloat(hexCode);
        break;
    case 28:
        resp.kMagY = hexToFloat(hexCode);
        break;
    case 29:
        resp.kMagZ = hexToFloat(hexCode);
        break;
    case 74:
        resp.kGyroX = hexToFloat(hexCode);
        break;
    case 75:
        resp.kGyroY = hexToFloat(hexCode);
        break;
    case 76:
        resp.kGyroZ = hexToFloat(hexCode);
        break;
    case 79:
        char ch;
        stringstream ss(hexCode);
        ss >> hex >> ch;
        resp.kHeadingStatus = ch;
        break;
    }
}

//перевод из 16ричной во float
float hexToFloat(string hexVal)
{
    //переводим строку в массив char
    char cstr[hexVal.size() + 1];
    hexVal.copy(cstr, hexVal.size() + 1);
    cstr[hexVal.size()] = '\0';

    //переодим массив char во float
    uint32_t num;
    float f;
    sscanf(cstr, "%x", &num);
    f = *((float*)&num);
    return f;
}

void printResponse(response resp)
{
    cout << "kHeading = " << resp.kHeading << " deg" << endl;
    cout << "kPitch = " << resp.kPitch << " deg" << endl;
    cout << "kRoll = " << resp.kRoll << " deg" << endl;
    cout << "kHeadingStatus = " << resp.kHeadingStatus << endl;
    cout << "kQuaternion = " << resp.kQuaternion[0] << " + " << resp.kQuaternion[1] << "i + " << resp.kQuaternion[2] << "j + " << resp.kQuaternion[3]<< "k" << endl;
    cout << "kTemperature = " << resp.kTemperature << " C" << endl;
    cout << "kDistortion = " << boolalpha << resp.kDistortion << endl;
    cout << "kCalStatus = " << boolalpha << resp.kCalStatus << endl;
    cout << "kAccelX = " << resp.kAccelX << " g" << endl;
    cout << "kAccelY = " << resp.kAccelY << " g" << endl;
    cout << "kAccelZ = " << resp.kAccelZ << " g" << endl;
    cout << "kMagX = " << resp.kMagX << " mT" << endl;
    cout << "kMagY = " << resp.kMagY << " mT" << endl;
    cout << "kMagZ = " << resp.kMagZ << " mT" << endl;
    cout << "kGyroX = " << resp.kGyroX << " rad/s" << endl;
    cout << "kGyroY = " << resp.kGyroY << " rad/s" << endl;
    cout << "kGyroZ = " << resp.kGyroZ << " rad/s" << endl;
}
